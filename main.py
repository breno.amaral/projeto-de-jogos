from random import seed

from werkzeug.utils import redirect
from util import *
from flask import Flask, render_template, request, session, redirect
app = Flask(__name__)
app.secret_key = 'super secret key'


'''
cada carta vai ser uma lista com o formato [elemento, numero]

elemento na forma de numero (gelo=0, fogo=1, agua=-1)
'''

@app.route('/')
def index():
	return render_template("index.html")

@app.route('/visualizar_cartas')
def visualizar_cartas(): #pagina que mostrara as cartas dos jogadores
	session['deck'] = gerar_deck()
	session['deck_adv'] = gerar_deck()
	deck = session['deck_adv']+session['deck']
	
	# embaralha e distribui as primeiras cartas ao jogador
	random.shuffle(session['deck'])
	random.shuffle(session['deck_adv'])
	session['cartas'] = session['deck'][:4]
	for i in range(4):
		session['deck'].pop(0)
	session['placar'] = [0,0,0,0,0,0]
	return render_template("visualizar_cartas.html", deck=deck)

@app.route('/partida', methods=['GET', 'POST'])
def partida(): #pagina que o jogador ira selecionar sua carta a cada rodada
	if not jogo_continua(session['placar']):
		return redirect('/fim_partida')
	
	session['cartas'] = session['cartas'] + [session['deck'][0]]
	session['deck'] = session['deck'][1:]
	return render_template("partida.html", cartas=session['cartas'], pontuacao=session['placar'])

@app.route('/resultado', methods=['GET', 'POST'])
def resultado(): #pagina que mostra o resultado de cada rodada
	# retira a carta selecioanda da mão do jogador e a coloca ao final do deck
	num_carta = request.form['carta_selecionada']
	carta_selecionada = session['cartas'][int(num_carta)]
	session['deck'] =  session['deck'] + [carta_selecionada]
	#session['cartas'] = session['cartas'].remove(carta_selecionada)
	session['cartas'].remove(carta_selecionada)
	# 'seleciona' a primeira carta do deck do adversario e a coloca ao final da lista
	carta_adversario = session['deck_adv'][0]
	session['deck_adv'] = session['deck_adv'][1:] + [session['deck_adv'][0]]

	if empate(carta_selecionada, carta_adversario):
		resultado = '"empate"'
	elif jogador_venceu(carta_selecionada, carta_adversario):
		resultado = '"vitoria"'
		session['placar'] = atualizar_placar(session['placar'], carta_selecionada)
	else:
		resultado = '"derrota"'
		session['placar'] = atualizar_placar(session['placar'], carta_adversario, vitoria_adv=True)

	return render_template("resultado.html", 
		carta_selecionada=carta_selecionada, 
		carta_adv=carta_adversario,
		resultado=resultado)

@app.route('/fim_partida', methods=['GET', 'POST'])
def fim_partida():
	if jogador_venceu_jogo(session['placar']):
		vencedor = "'jogador'"
	else:
		vencedor = "'adversario'"
	return render_template('fim_partida.html', vencedor=vencedor)