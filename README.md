# Projeto de Jogos

> Revision: 0.0.1
<p align="center">
   <img src="http://img.shields.io/static/v1?label=STATUS&message=EM%20DESENVOLVIMENTO&color=RED&style=for-the-badge"/>
</p>

...

Sumário
    
Ideia Principal e Regras
3
Principais Mecânicas
3
Público Alvo
4
Controles
4
Interface
5
Desenvolvimento
9
Considerações Finais
9























Ideia Principal e Regras
    Projeto: "MagiCards"

    Gerar uma batalha 1vs1 onde a cada rodada o jogador deve selecionar uma das 5 cartas que possui para “enfrentar” a carta do oponente. O enfrentamento segue a lógica do “pedra, papel ou tesoura”, onde cada um desses atributos vence de um, mas perde do outro. No entanto, agora há mais uma mecânica de desempate quando os atributos forem iguais - o nível de carta (como trunfo).

    Os atributos agora são representados por fogo, gelo e água, onde fogo vence gelo, gelo vence água e água vence fogo. E os níveis variam de 1 a 10.

    O placar do jogo é dividido de duas formas:
Primeiro separa-se a pontuação do jogador 1 do jogador 2
Depois há fragmentações na pontuação de cada jogador, onde há contadores para as 3 divisões que correspondem a cada elemento. Ou seja, cada jogador possui pontos de fogo, pontos de gelo e pontos de água.

A cada duelo (enfrentamento) de cartas, o vencedor soma um ponto no placar do elemento da carta escolhida.

Vence o jogo o primeiro a conseguir 3 pontos do mesmo elemento ou pelo menos 1 ponto de cada elemento.    


Principais Mecânicas

1 - Algoritmo de montagem de baralho: 
Ao início da partida, cada jogador recebe 10 cartas, cujos níveis variam de 1 a 10 (e não se repetem). Destas 10 cartas, no mínimo 2 delas são de cada um dos 3 elementos, e as 4 restantes são decididas aleatoriamente.

2 - Algoritmo de embaralhamento:
    As 10 cartas que o jogador possui são embaralhadas aleatoriamente, formando uma fila (sequência) entre elas. A cada rodada, a “mão” do jogador (ou seja, as cartas que estão disponíveis para serem jogadas nesta rodada) será composta pelas 5 primeiras cartas da fila.
    Quando escolhemos uma carta para o duelo, independente do resultado, ela vai para o final da fila e a “mão” é substituída pela 6ª carta da fila.

3 - Escolha da carta:
    A cada rodada, o jogador deverá selecionar uma carta de sua “mão” para que esta seja utilizada no duelo. Obviamente, o jogador não sabe qual a carta escolhida pelo adversário até que realize sua jogada

4 - Partida contra a máquina:
Ao enfrentarmos nosso adversário robô, ele submete-se às mesmas instruções que o jogador, tanto na montagem do baralho quanto no embaralhamento. E quando ele deve escolher uma entre as 5 cartas em sua “mão”, faz isso de forma aleatória. Ainda assim, mesmo que não haja uma estratégia da máquina, o jogo consegue proporcionar um desafio divertido.



Público Alvo

    Jogadores casuais de qualquer idade que buscam jogos simples, rápidos e divertidos. Foco em jogadores que gostem de cartas e tenham certa nostalgia de minigames simples com essa mecânica. Basicamente, jogadores de truco.




Controles

Jogo em estilo “clicker”, ou seja, só precisa do mouse para interagir com o jogo, todos os comandos são feitos com clicks.


Interface
Ao iniciar sua aventura nas tabernas virtuais onde vivem os seres jogadores de MagiCards, aqui encontramos em face a primeira tela inicial, o Menu:


 
    Seguindo um aspecto mais direto, podemos visualizar em destaque a palavra JOGAR, logo abaixo do nome do projeto, e ao lado temos as instruções necessárias para entender as regras do jogo. Ao simples apertar de um botão já teremos a primeira tela de jogo, a distribuição de decks:



    Nesta etapa, a interface  apresenta suas cartas e de seu adversário, mostrando um momento em que você pode visualizar as 10 cartas do seu deck e do seu oponente, dando a liberdade de elaborar alguma estratégia antes da primeira distribuição de cartas, que a propósito:



    Nesta interface temos o início da primeira rodada de uma partida de MagiCards,  em que foram selecionadas aleatoriamente 5 cartas do seu deck mostrado anteriormente, em seguida deve-se selecionar a carta:


 
    Selecionando ela deve-se agora fazer sua jogada, através do botão em destaque na interface, com isso, teremos o resultado da rodada apresentado em uma nova tela, onde finalmente nos é revelado a carta que o adversário selecionou:



    Seguido do resultado, teremos a interface em uma nova rodada com os pontos atualizados:



    Ao escalar da partida de MagiCards, uma hora será atingida a meta necessária para ganhar o jogo, seja pra você ou para seu adversário, e enfim teremos o resultado apresentado nesta última interface:



    Podemos visualizar mais de perto a interface acessando o vídeo de apresentação geral do Projeto: MagiCards.



Desenvolvimento

A ideia bruta do jogo veio da nostalgia de Club Penguin assim como BLTD, então mesclamos ambos. A idéia inicial era usar unreal engine à fim de copiar o visual “semi 3d” de jogos tcg (trading card game e afins) modernos porém tal tarefa se mostrou muito difícil e fora do alcance. Principalmente a dificuldade de fazer set up da Unreal assim como a curva de aprendizado de softwares para modelagem de jogos. Também era cogitado desenvolvermos uma IA super simples para jogar contra o jogador, o que se mostrou complexo demais. 
Terminamos por fazer uma versão mais simples que roda usando a biblioteca Flask que integra desenvolvimento web front end com python, devido a experiência rasa, contudo presente que certos membros do grupo já tinham com a biblioteca finalizamos um protótipo do jogo deste modo. 
Uma outra opção não terminada seria poder criar um duelo pvp que gera um link compartilhável com qualquer pessoa na internet que pode aceitar tal duelo.
O código-fonte do projeto pode ser encontrado em https://gitlab.com/breno.amaral/projeto-de-jogos/.


Considerações Finais

    Por fim, mencionaremos aspectos que gostaríamos de ter implementado ao jogo, mas por falta de tempo não foi possível:
    Pretendíamos criar um sistema de desbloqueio de cartas, assim como criar diferentes modos de jogo que escalassem a dificuldade da partida e desbloqueio de itens cosméticos. No entanto, não foi possível. Sendo assim, o MagiCards não possui um sistema de progressão. Isso também afetou nosso público alvo, já que adequa-se mais ao jogador casual do que um jogador assíduo.
    Gostaríamos de ter implementado um sistema com inteligência artificial no modo contra máquina, ao invés de apenas fazer com que ela selecionasse cartas aleatoriamente. E também implementar um modo multiplayer, tanto local quanto via rede.
    Também queríamos apresentar uma interface mais complexa e divertida, tentando trazer personagens macacos ao jogo e representar as cartas por bananas. A ideia era utilizar unreal engine para esta parte, até tentamos por um período, mas logo vimos que não seria viável naquele momento.

## Desenvolvedores

Time responsável pelo desenvolvimento do projeto:

- RA: 21907601 [Breno Sanchi](https://gitlab.com/breno.amaral)
- RA: 21906410 [Diego Figueiró](https://gitlab.com/diego.figueiro)
- RA: 21900953 [Gabriel Neves](https://gitlab.com/gabriel.neves)
- RA: 21000000 [Jediael](https://gitlab.com/)
- RA: 21907417 [Shivaram Palackapillil](https://gitlab.com/shivaram.agnijan)


[⬆ Voltar ao topo](#projeto-de-jogos)<br>
