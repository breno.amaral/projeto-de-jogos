import random
import re

def gerar_deck():
    # cria deck com 2 elementos de cada
    elementos = [0, 0, 1, 1, -1, -1]
    # completa com mais 4 elemtentos aleatoriamente
    for i in range(4):
        elementos.append(random.choice([-1,0,1]))
    #embaralha a ordem e coloca os valores de 1 a 10
    random.shuffle(elementos)
    return [[elementos[i], i+1] for i in range(10)]

def empate(jogador, adversario):
    return jogador==adversario

def jogador_venceu(jogador, adversario):
    if jogador[0]!=adversario[0]: #verifica a igualdade de elementos
        return (jogador[0]==-1 and adversario[0]==1 # agua contra fogo
            or jogador[0]==1 and adversario[0]==0 # fogo contra gelo
            or jogador[0]==0 and adversario[0]==-1) # gelo contra agua
    return jogador[1] > adversario[1]

def atualizar_placar(placar, carta, vitoria_adv=False):
    elementos = [-1, 1, 0]
    if vitoria_adv:
        placar[elementos.index(carta[0])] += 1
    else:
        placar[elementos.index(carta[0])+3] += 1
    return placar

def jogo_continua(placar):
    return 3 not in placar and 0 in placar[:3] and 0 in placar[3:]

def jogador_venceu_jogo(placar):
    return 3 in placar[3:] or 0 not in placar[3:]