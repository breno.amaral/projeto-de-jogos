# Criando a classe "carta"
class carta:
    # Inicializando os 2 parâmetros básicos - atributo e nível
    def __init__(self, atributo_nome, nivel):
        self.atributo_nome = atributo_nome
        # O "atributo_forca" carregará um inteiro que será usado durantea batalha para definir o vencedor
        self.atributo_forca = at_fo(atributo_nome)
        self.nivel = nivel

# Função que atribui o respectivo inteiro ao "atributo_forca"
def at_fo(atributo_nome):
    if atributo_nome == 'gelo':
        return 0
    elif atributo_nome == 'fogo':
        return 1
    else: # 'agua'
        return -1

# Função que determina o vencedor do duelo (qual carta venceu)
# E retorna a posição do vencedor (1 ou 2) e o atributo
def juiz(carta1, carta2):
    # If que compara os níveis das cartas, caso tenham mesmo atributo
    if carta1.atributo_forca == carta2.atributo_forca:
        # Verifica quem tem o maior "nivel" e retorna o vencedor (ou empate)
        if carta1.nivel == carta2.nivel:
            print('Empate')
            # Retorna 2, valor que corresponde a nenhum jogador, indicando empate
            return (2, 'gelo')
        elif carta1.nivel > carta2.nivel:
            print('Carta 1 venceu')
            # Retorna o jogador vencedor (jogador1==0 e jogador2==1) e o atributo - será usado no placar
            return (0, carta1.atributo_nome)
        else:
            print('Carta 2 venceu')
            return (1, carta2.atributo_nome)

    else:
        # Verifica se há duelo entre "fogo" (carta1) e "agua" (carta2) para adequação do "atributo_forca"
        if (carta1.atributo_forca == 1) and (carta2.atributo_forca == -1):
            # Corrige o "atributo_forca" de "agua", para que este atributo ganhe de "fogo"
             carta2.atributo_forca += 3
        # Verifica se há duelo entre "agua" (carta1) e "fogo" (carta2) para adequação do "atributo_forca"
        elif (carta1.atributo_forca == -1) and (carta2.atributo_forca == 1):
            # Corrige o "atributo_forca" de "fogo", para que este atributo perca de "agua"
            carta2.atributo_forca -= 3

        # Caso a "carta1" for "gelo", não é necessário adequar o "atributo_forca"

        # Verifica quem tem o maior "atributo_forca" e retorna o vencedor
        if carta1.atributo_forca > carta2.atributo_forca:
            print('Carta 1 venceu')
            return (0, carta1.atributo_nome)
        else:
            print('Carta 2 venceu')
            return (1, carta2.atributo_nome)


''' PLACAR'''



# Função que altera o placar. Recebe o próprio placar, o jogador que pontuou e o atributo da carta
def placar(plac,jogador, atributo):
    # Verifica se o vencedor é o jogador 1 ou 2. Se não for nenhum deles, deu empate e nada ocorre
    if jogador in [0,1]:
        # Incrementa o atributo do jogador vencedor no placar
        plac[jogador][atributo] += 1



''' BARALHO '''

# Importando biblioteca random
import random

# Função que cria o baralho e o retorna embaralhado
def baralho(num):
    # Cria listas que serão usadas na função
    deck = []
    atr_nom = ['fogo', 'gelo', 'agua']
    lis_atr = []

    # Cria a lista ['fogo', 'gelo', 'agua', 'fogo', 'gelo', 'agua', 'fogo', 'gelo', 'agua']
    for i in range(3):
        for j in range(3):
            lis_atr.append(atr_nom[j])

    # Embaralha essa lista
    random.shuffle(lis_atr)

    # Cria cartas de 1 a 9, sendo 3 de cada atributo, e as coloca no baralho (em forma de lista)
    for i in range(1, 10):
        deck.append(carta(lis_atr[i-1], i))

    # Adiciona a carta 10 com um dos atributos aleatoriamente ao baralho
    deck.append(carta( random.choice(atr_nom), 10))

    print('\n \n \n \n')
    # Printa as cartas e sua ordem no baralho
    print("Baralho do jogador", num, '\n')
    for i in range(10):
        print(i+1, '-', deck[i].atributo_nome, deck[i].nivel)

    # Embaralha o baralho
    random.shuffle(deck)


    str(input(''))
    # Retorna o baralho
    return deck

# Função do confronto multijogador local. Recebe o deck e pede ao jogador fazer a jogada
def confronto_jog(deck, num):

    # Mostra as cartas que o jogador tem na mão
    for i in range(5):
        print(i + 1, '-', deck[i].atributo_nome, deck[i].nivel)

    # Pede para o jogador selecionar a carta
    if num == 1:
        entro = int(input('\n Jogador 1 - Digite o número da carta selecionada: '))
    else:
        entro = int(input('\n Jogador 2 - Digite o número da carta selecionada: '))

    # Retira a carta escolhida e a coloca no final do baralho
    deck.append(deck[entro-1])
    deck.remove(deck[entro-1])

    print('\n \n \n \n \n')


# Função da jogada do bot. Recebe o deck
def confronto_bot(deck):
    # Mostra as cartas que o jogador tem na mão
    for i in range(5):
        print(i + 1, '-', deck[i].atributo_nome, deck[i].nivel)

    # Escolhe uma das 5 cartas aleatoriamente
    entro = random.choice([1,2,3,4,5])
    print(entro)

    # Retira a carta escolhida e a coloca no final do baralho
    deck.append(deck[entro-1])
    deck.remove(deck[entro-1])

    print('\n \n \n \n \n')





# Função que executa a partida por completo
# Recebe um booleano que indica se a partida é solo (True), ou multijogador (False)
def partida(solo):

    # Criando o placar em lista. O jogador 1 é a posição [0] e o jogador 2 é a posição [1]
    # A pontuação do jogador está em dicionário, onde o nome do atributo é sua chave
    plac = [{'fogo': 0, 'gelo': 0, 'agua': 0}, {'fogo': 0, 'gelo': 0, 'agua': 0}]

    # Cria os baralhos do jogadores
    deck1 = baralho(1)
    deck2 = baralho(2)

    # Criando booleano para usá-lo no while
    logi = False
    # Criando o while
    while logi == False:

        # Jogada do jogador 1
        confronto_jog(deck1, 1)

        # Verifica se a partida é solo ou multijogador local para executar a jogada do jogador 2
        if solo != True:
            confronto_jog(deck2, 2)
        else:
            confronto_bot(deck2)

        # Aplica o resultado do duelo em "res"
        res = juiz(deck1[9], deck2[9])

        # Aplica o jogador e carta na função placar para atualização do resultado
        placar(plac, res[0], res[1])
        # Printa o placar atualizado
        print('\n', 'PLACAR - Jogador 1 / Jogador 2')
        print(plac)

        # Verifica se algum jogador venceu a partida
        # For que passa por ambos jogadores
        for i in range(0,2):
            # Criando e zerando contador usado para verificar se há pontos nos 3 atributos (condição de vitória)
            cont = 0
            # For que percorre os elementos do dicionário (atributos)
            for j in plac[i]:
                # Verifica se o jogador tem 3 pontos do mesmo atributo (condição de vitória)
                if plac[i][j] == 3:
                    # Se sim, o jogador venceu e paramos o while alterando "logi"
                    print('Jogador', i+1, 'venceu')
                    logi = True

                # Verifica se há mais de um ponto no atributo
                elif plac[i][j] > 0:
                    # Se sim, incrementa o contador
                    cont += 1

                # Verifica se o contador é igual 3, ou seja, se o jogador possui pontos em todos os atributos
                if cont == 3:
                    # Se sim, o jogador venceu e paramos o while alterando "logi"
                    print('Jogador', i+1, 'venceu')
                    logi = True



# Função que executa a partida por completo
# Recebe um booleano que indica se a partida é solo (True), ou multijogador (False)
partida(True)
